## Implementation d'un réseau Yolov3 en python et développement d'un outil d'aide à la création de dataset  

Fichier principal : train.py

Ce projet vise à développer un réseau Yolov3 pour la compétition Robocup Logistics League afin d'améliorer la prise de décision par les Robotino.
L'objectif est d'entraîner le réseau à reconnaître différents objets à partir des images fournies par la caméra.
On chercher notamment à détecter des humains, des machines d'usinage et des Robotinos.
Nous avons entraîné le réseau sur un premier dataset afin de vérifier le bon fonctionnement de l'implémentation en Python du réseau Yolov3.
Nous expliquons ci-dessous la démarche à suivre afin d'entraîner le réseau sur un autre dataset.

### Utilisation d'un autre dataset

Il est préférable de placer le dataset choisi dans le répertoire ./data/nouveau_dataset  
nouveau_dataset est un repertoire contenant les elements suivants :  
- un répertoire "images" contenant toutes les images
- un répertoire "labels" contenant tous les labels (Dans notre cas, les labels sont
des fichiers .txt portant le nom de l'image qu'ils décrivent et contenant la description des bounding boxes de l'image)
- un fichier train.txt contenant le chemin d'accès à toutes les images
 utilisées pour l'apprentissage (training set)
- un fichier val.txt similaire à train.txt mais pour l'évaluation (validation set)

On pourra s'aider du programme createlist.py pour générer les fichiers train.txt et val.txt

Il faut ensuite modifier plusieurs fichiers :

#### ./config/coco.data :

classes=num_classes , num_classes le nombre de classes dans le dataset (ex : humain, robot, machine => classes=3)
train=chemin_train , chemin_train le chemin d'accès vers le fichier train.txt
valid=chemin_valid , chemin_valid le chemin d'accès vers le fichier val.txt


#### ./config/coco.names :

Dans ce fichier, il faut indiquer chaque nom de classe, un nom par ligne.
La première ligne décrit le nom de la classe 0, la deuxième celui de la classe 1, etc.
Il faut un total de num_classes lignes


#### ./config/yolov3.cfg : 

Dans la première section [net], ajuster la taille de batch et subdivision pour s'adapter au GPU utilisées.
Possibilité de régler le learning rate également.

Dans les trois sections [yolo] du fichier, modifier la valeur de "classes" de telle sorte que classes=num_classes
Dans chaque section [convolutional] qui se trouve juste au dessus de [yolo], modifier la valeur de "filters" de telle sorte que filters=((num_classes + 5) x 3)


#### ./train.py :

Modifier la valeur par défaut de l'argument image_folder par le chemin d'accès à votre répertoire "images"
Modifier id_to_name conformément à ./config/coco.names  (pas indispensable)


Une fois ces modifications effectuées, le programme devrait fonctionner avec le nouveau dataset

