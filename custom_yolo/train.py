from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *
from utils.parse_config import *
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from PIL import Image
import glob
import random

import os
import sys
import time
import datetime
import argparse

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim

id_to_name = ("0", "1", "+4", "+2", "reverse", "skip_turn",
              "choose_color", "2", "3", "4", "5", "6", "7", "8", "9")

lossGraph = "./lossgraph.txt"
# nms_thres=0.4
# conf_thres=0.8

weights_files = sorted(glob.glob('./checkpoints/*.*'))
for i in range(len(weights_files)):
    weights_files[i] = weights_files[i].split('/')[-1]

parser = argparse.ArgumentParser()
parser.add_argument("--mode", type=str, default="train",
                    help="mode de fonctionnement du reseau: train, valid")
parser.add_argument("--show", type=int, default=0,
                    help="affiche i images traitées par yolov3")
parser.add_argument("--epochs", type=int, default=1, help="number of epochs")
parser.add_argument("--image_folder", type=str,
                    default="data/uno/images", help="path to dataset")
parser.add_argument("--batch_size", type=int, default=8,
                    help="size of each image batch")
parser.add_argument("--model_config_path", type=str,
                    default="config/yolov3.cfg", help="path to model config file")
parser.add_argument("--data_config_path", type=str,
                    default="config/coco.data", help="path to data config file")
parser.add_argument("--weights_path", type=str, default="./checkpoints/" +
                    weights_files[-1], help="path to weights file")
parser.add_argument("--class_path", type=str,
                    default="config/coco.names", help="path to class label file")
parser.add_argument("--conf_thres", type=float, default=0.8,
                    help="object confidence threshold")
parser.add_argument("--nms_thres", type=float, default=0.4,
                    help="iou thresshold for non-maximum suppression")
parser.add_argument("--n_cpu", type=int, default=0,
                    help="number of cpu threads to use during batch generation")
parser.add_argument("--img_size", type=int, default=416,
                    help="size of each image dimension")
parser.add_argument("--checkpoint_interval", type=int,
                    default=1, help="interval between saving model weights")
parser.add_argument("--checkpoint_dir", type=str, default="checkpoints",
                    help="directory where model checkpoints are saved")
parser.add_argument("--use_cuda", type=bool, default=True,
                    help="whether to use cuda if available")
opt = parser.parse_args()
print(opt)

mode = opt.mode

cuda = torch.cuda.is_available() and opt.use_cuda
print("Cuda status : " + str(cuda))
os.makedirs("checkpoints", exist_ok=True)

classes = load_classes(opt.class_path)

# Get data configuration
data_config = parse_data_config(opt.data_config_path)
train_path = data_config["train"]
valid_path = data_config["valid"]

# Get hyper parameters
hyperparams = parse_model_config(opt.model_config_path)[0]
learning_rate = float(hyperparams["learning_rate"])
momentum = float(hyperparams["momentum"])
decay = float(hyperparams["decay"])
burn_in = int(hyperparams["burn_in"])


# Initiate model
model = Darknet(opt.model_config_path)
model.load_weights(opt.weights_path)
# model.apply(weights_init_normal)

if cuda:
    model = model.cuda()

# only compute last layers
total_param = 0
for param in model.parameters():  # permet de savoir combien de couches comporte le reseau
    total_param += 1


print("Nombre de couches du reseau : " + str(total_param))
tmp = 0
for param in model.parameters():  # on frise toutes les couches avant les 19 dernieres
    if tmp <= total_param-5:
        param.requires_grad = False

    tmp += 1

start = time.time()

# Analytics
total_loss = 0.
total_precision = 0.


if mode == "valid":
    # cas où --show=0
    if opt.show >= 1:
        model.eval()
        print("----------------MODE EVALUATION----------------")

        # Get bounding-box colors
        cmap = plt.get_cmap('tab20b')
        colors = [cmap(i) for i in np.linspace(0, 1, 20)]

        # Get dataloader
        dataloader = torch.utils.data.DataLoader(
            # ListDataset(valid_path), batch_size=1, shuffle=False, num_workers=opt.n_cpu
            ListDataset(train_path), batch_size=1, shuffle=True, num_workers=opt.n_cpu)

        Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

        index = 0
        for batch_i, (path, imgs, label) in enumerate(dataloader):
            index += 1
            if index <= opt.show:
                imgs = Variable(imgs.type(Tensor))

                img = np.array(Image.open(path[0]))
                plt.figure()
                fig, ax = plt.subplots(1, figsize=(12, 9))
                ax.imshow(img)

                pad_x = max(img.shape[0] - img.shape[1],
                            0) * (opt.img_size / max(img.shape))
                pad_y = max(img.shape[1] - img.shape[0],
                            0) * (opt.img_size / max(img.shape))
                unpad_h = opt.img_size - pad_y
                unpad_w = opt.img_size - pad_x

                with torch.no_grad():
                    detect = model(imgs)
                    detect = non_max_suppression(
                        detect, 15, opt.conf_thres, opt.nms_thres)
                detections = detect[0]

                if detections is not None:
                    unique_labels = detections[:, -1].cpu().unique()
                    # print(unique_labels)
                    print()
                    print("----------DECTECTION: %s----------" % (path))
                    for name in unique_labels:
                        print(id_to_name[int(name.item())])
                        n_cls_preds = len(unique_labels)
                        bbox_colors = random.sample(colors, n_cls_preds)
                        # browse detections and draw bounding boxes
                        for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:
                            box_h = ((y2 - y1) / unpad_h) * img.shape[0]
                            box_w = ((x2 - x1) / unpad_w) * img.shape[1]
                            y1 = ((y1 - pad_y // 2) / unpad_h) * img.shape[0]
                            x1 = ((x1 - pad_x // 2) / unpad_w) * img.shape[1]
                            color = bbox_colors[int(
                                np.where(unique_labels == int(cls_pred))[0])]
                            bbox = patches.Rectangle(
                                (x1, y1), box_w, box_h, linewidth=2, edgecolor=color, facecolor='none')
                            ax.add_patch(bbox)
                            plt.text(x1, y1, s=classes[int(cls_pred)], color='white', verticalalignment='top', bbox={
                                     'color': color, 'pad': 0})
                    plt.axis('off')
                    #plt.savefig(img_path.replace(".jpg", "-det.jpg"), bbox_inches='tight', pad_inches=0.0)
                    plt.show()
            else:
                break

    # cas où --show est nul
    else:
        model.eval()
        print("----------------MODE EVALUATION----------------")

        # Get dataloader
        dataloader = torch.utils.data.DataLoader(ListDataset(
            valid_path), batch_size=opt.batch_size, shuffle=False, num_workers=opt.n_cpu)

        Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

        for batch_i, (_, imgs, targets) in enumerate(dataloader):
            imgs = Variable(imgs.type(Tensor))
            targets = Variable(targets.type(Tensor),
                               requires_grad=False)  # BoolTensor
            loss = model(imgs, targets)
            total_loss += loss.item()
            total_precision += model.losses["precision"]
            print("[Batch %d/%d] [Losses: x %f, y %f, w %f, h %f, conf %f, cls %f, total %f, recall: %.5f, precision: %.5f]"
                  % (
                      (batch_i+1),
                      len(dataloader),
                      model.losses["x"],
                      model.losses["y"],
                      model.losses["w"],
                      model.losses["h"],
                      model.losses["conf"],
                      model.losses["cls"],
                      loss.item(),
                      model.losses["recall"],
                      model.losses["precision"],
                  )
                  )

        print("[Mean Loss: %f, Mean Precision: %f]" %
              (total_loss/len(dataloader), total_precision/len(dataloader)))

else:

    index = getFileLastIndex(lossGraph)+1
    model.train()
    print("----------------MODE APPRENTISSAGE-----------------")

    # Get dataloader
    dataloader = torch.utils.data.DataLoader(
        ListDataset(train_path), batch_size=opt.batch_size, shuffle=False, num_workers=opt.n_cpu
    )

    Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor

    optimizer = torch.optim.Adam(
        filter(lambda p: p.requires_grad, model.parameters()))
    for epoch in range(opt.epochs):
        for batch_i, (_, imgs, targets) in enumerate(dataloader):
            imgs = Variable(imgs.type(Tensor))
            targets = Variable(targets.type(Tensor),
                               requires_grad=False)  # BoolTensor

            optimizer.zero_grad()

            loss = model(imgs, targets)

            loss.backward()
            optimizer.step()
            total_loss += loss.item()
            total_precision += model.losses["precision"]

            print(
                "[Epoch %d/%d, Batch %d/%d] [Losses: x %f, y %f, w %f, h %f, conf %f, cls %f, total %f, recall: %.5f, precision: %.5f]"
                % (
                    (epoch+1),
                    opt.epochs,
                    (batch_i+1),
                    len(dataloader),
                    model.losses["x"],
                    model.losses["y"],
                    model.losses["w"],
                    model.losses["h"],
                    model.losses["conf"],
                    model.losses["cls"],
                    loss.item(),
                    model.losses["recall"],
                    model.losses["precision"],
                )
            )

            model.seen += imgs.size(0)

        if epoch % opt.checkpoint_interval == 0:
            #model.save_weights("%s/%d.weights" % (opt.checkpoint_dir, epoch))
            model.save_weights("%s/%s.weights" % (opt.checkpoint_dir,
                                                  datetime.datetime.now().strftime("%y_%m_%d_%H:%M:%S")))

        print("[Epoch %d/%d] [Mean Precision: %f, Mean Loss: %f]" % ((epoch+1),
                                                                     opt.epochs, total_precision/len(dataloader), total_loss/len(dataloader)))
        fout = open(lossGraph, "a+")
        fout.write("%d %f %f\n" % (index, total_precision /
                                   len(dataloader), total_loss/len(dataloader)))
        index += 1
        total_precision = 0
        total_loss = 0
    fout.close()

print("Temps d'execution : %s sec" % (time.time()-start))
